class Echo {
  private:
    int trigPin = 8; 
    int echoPin = 9; 
    long duration, cm, inches;
    long distance;
    int tries = 0;
    int avg = 0;
    int loopcnt = 0;
    int maxloop = 10;
    long sum = 0;

    int delayAfterReading = 10;
    int delayTimeout = 20;

   public:
    Echo(int, int, int);
    int getDistance();  
    String getRadar(int);
    int maxDistance = 150;
    
};

Echo::Echo(int triggerPin, int echoPin, int maxDist) {
  this->trigPin = triggerPin;
  this->echoPin = echoPin;
  this->maxDistance = maxDist;
}

int Echo::getDistance() {
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  cm = -1;
  int minVal=1023;
  int maxVal=-1;
  avg = 0;
  sum = 0;
  int readings = 0;
  
  pinMode(echoPin, INPUT);
  for(int i=0; i<maxloop; i++){
    // Read the signal from the sensor: a HIGH pulse whose
    // duration is the time (in microseconds) from the sending
    // of the ping to the reception of its echo off of an object.
      digitalWrite(trigPin, LOW);
      delayMicroseconds(5);
      digitalWrite(trigPin, HIGH);
      delayMicroseconds(12);
      digitalWrite(trigPin, LOW);

    duration = pulseIn(echoPin, HIGH, maxDistance*90);
    //Serial.println(duration);
    if (duration == 0 ){
      // Out of range
      //Serial.print("?");      
      pinMode(echoPin, OUTPUT);
      digitalWrite(echoPin, LOW);
      delay(50);
      pinMode(echoPin,INPUT);
      return cm;
      delay(delayAfterReading);
    }  else {
      //Serial.print("!");
      // convert the time into a distance
      cm = (duration/2) / 29.1;    
      delay(delayAfterReading);
      minVal = cm < minVal ? cm : minVal;
      maxVal = cm > maxVal ? cm : maxVal;  
      sum+=cm;
      readings++;
    }    
  }
  //avg = (sum-minVal-maxVal)/(maxloop-2);
  if (sum>0){
    avg = sum/readings;
    cm = avg;
  } else {
    return -1;
  }
  return cm;
  if( cm < maxDistance ) {
    return cm;
  }   
  else {
    return -1;
  } 
   
}

String Echo::getRadar(int maxLength = 20){
  String out = "";
  int dist = this->getDistance();
  dist = map(dist, 0, this->maxDistance, 0, maxLength);
  for(int i=0; i<dist; i++){
     out += ".";
  }
  return out;
}

