#include "Echo.h"

int trigPin = 8; 
int echoPin = 9; 

int maxDist = 150;  // distanza massima in cm

Echo echo = Echo(trigPin, echoPin, maxDist);

void setup() {
  Serial.begin(9600);
  Serial.println("Ultrasonic radar.");
  Serial.println("Max distance: " + String(echo.maxDistance));
  //Define inputs and outputs
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void loop() {
   int cm = echo.getDistance();
   if(cm>=0){
    Serial.println("cm: " + String(cm));
    Serial.println(echo.getRadar(50));
   } else {
    Serial.println("?");
   }
}
